///////////////////////////////////////////////////////////////////////////////
// Filename: BallParticle.cpp
////////////////////////////////////////////////////////////////////////////////

#include "BallParticle.h"

/**
 * Constructor for the BallParticle
 */
BallParticle::BallParticle()
	: BaseParticle()
{
	m_maxTravel = 0.0f;
	m_hasMaxTravel = false;
	m_isStillMoving = true;
}

/**
 * Copy Constructor for the BallParticle
 *
 * copy		BallParticle to copy
 */
BallParticle::BallParticle(const BallParticle &copy)
	: BaseParticle(copy)
{
	m_maxTravel = copy.m_maxTravel;
	m_hasMaxTravel = copy.m_hasMaxTravel;
	m_isStillMoving = copy.m_isStillMoving;
}

/**
 * Destructor for the Ball
 */
BallParticle::~BallParticle()
{ }

/**
 * Initialize the Particle
 *
 * @param posX			X position of the particle
 * @param posY			Y position of the particle
 * @param posZ			Z position of the particle
 * @param velX			Velocity in the X direction of the particle
 * @param velY			Velocity in the Y direction of the particle
 * @param velZ			Velocity in the Z direction of the particle
 * @param initColorR	Initial Red value for the color of the particle
 * @param initColorG	Initial Green value for the color of the particle
 * @param initColorB	Initial Blue value for the color of the particle
 * @param finalColorR	Final Red value for the color of the particle
 * @param finalColorG	Final Green value for the color of the particle
 * @param finalColorB	Final Blue value for the color of the particle
 * @param lifetime		Total lifetime for the particle
 * @param maxTravel		Max distance of travel for the particle
 */
bool BallParticle::Initialize(float posX, float posY, float posZ, float velX, float velY, float velZ, float speed, float initColorR, float initColorG, float initColorB, float finalColorR, float finalColorG, float finalColorB, float lifetime, float maxTravel)
{
	m_position.X = posX;
	m_position.Y = posY;
	m_position.Z = posZ;

	m_velocity.X = velX;
	m_velocity.Y = velY;
	m_velocity.Z = velZ;
	m_speed = speed / 100;

	m_color.Initialize(initColorR, initColorG, initColorB, finalColorR, finalColorG, finalColorB);

	m_age = 0.0f;
	m_lifetime = lifetime;
	m_isAlive = true;

	m_maxTravel = maxTravel;
	m_hasMaxTravel = true;
	m_isStillMoving = true;

	return true;
}

bool BallParticle::Update(float timeElapsed)
{
	//PRETEND WE ARE SUBTRACTING THE PARTICLE POSITION FROM THE ORIGIN OF THE BALL
	Vector3f vector = m_position;
	float distance = vector.Length();

	/**************   BaseParticle Update   ***********************/
	//Why not just call the base class? It is slightly modifed in this case.
	
	//If the particle is still alive
	if (m_age < m_lifetime)
	{
		//Update the age of the particle
		m_age += timeElapsed;

		//Only update the position if the particle is not at the maxDistance (and we have to adhere to the max distance)
		if (!m_hasMaxTravel || (m_hasMaxTravel && distance < m_maxTravel))
		{
			//Update the position of the particle depending on the velocity
			m_position.X += m_velocity.X * timeElapsed * m_speed;
			m_position.Y += m_velocity.Y * timeElapsed * m_speed;
			m_position.Z += m_velocity.Z * timeElapsed * m_speed;
		}

		//Update the alpha value for the particle
		m_color.SetAlpha(1.0f - (m_age / m_lifetime));
	}
	else
	{
		//Particle is dead. Kill it.
		m_isAlive = false;
	}

	/**************************************************************/

	//Check, if we have a max travel distance, if we have hit thus travel distance
	if (m_hasMaxTravel)
	{
		//If we have hit the ball radius
		if (distance > m_maxTravel)
		{
			//Reset the position of the particle to the radius mark
			m_position.Normalize();
			m_position *= m_maxTravel;
			m_isStillMoving = false;
		}

		//Else, while we are getting lose to the radius, start slowing down the particle
		else if (distance >= m_maxTravel * 0.75f && m_speed >= 0.002f)
			m_speed = m_speed * 0.9f;
	}

	//Calculate the color of the particle
	if (m_hasMaxTravel)
		m_color.UpdateColor(distance / m_maxTravel);
	else
		m_color.UpdateColor(1.f - (distance - m_maxTravel / m_maxTravel));

	return true;
}

/**
 * Break the Particle free of the oppressive reigns of the Maximum Distance!
 */
bool BallParticle::BreakFree()
{
	//Set it free!
	m_hasMaxTravel = false;

	//Tell the world!
	m_isStillMoving = true;

	return true;
}
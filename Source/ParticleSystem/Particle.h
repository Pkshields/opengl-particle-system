////////////////////////////////////////////////////////////////////////////////
// Filename: Particle.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include <Windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "InterpolatedColor.h"
#include "Vector3f.h"
#include "BaseParticle.h"

class Particle : public BaseParticle
{
public:
	Particle();
	Particle(const Particle &copy);
	~Particle();

	bool Initialize(float posX, float posY, float posZ, float velX, float velY, float velZ, float speed, float colorR, float colorG, float colorB, float colorA, float lifetime);
};
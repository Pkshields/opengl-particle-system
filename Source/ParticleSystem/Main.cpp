////////////////////////////////////////////////////////////////////////////////
// Filename: Main.cpp
////////////////////////////////////////////////////////////////////////////////

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "TextureLoader.h"
#include "ParticleBall.h"

ParticleBall** particleBall;
int ballCount;
GLuint textures[3];

#define IS_FULLSCREEN false


enum MenuOptions
{
	START_ESCAPING_PARTICLES,
	STOP_ESCAPING_PARTICLES, 

	BALL_COUNT_1,
	BALL_COUNT_2,
	BALL_COUNT_3,
	
	ROTATION_SLOW,
	ROTATION_MED,
	ROTATION_FAST, 
	
	TEXTURE_CLOUD, 
	TEXTURE_BUBBLE, 
	TEXTURE_SPARK,

	COLOR_SET_1,
	COLOR_SET_2,
	COLOR_SET_3, 

	CLOSE_WINDOW
};

/**
 * Call to Draw Scene
 */
void RenderScene(void)
{
	//Clears the buffersm with "GL_COLOR_BUFFER_BIT" enabling the buffer for colour writing
	//Now also clears the Depth Buffer
	//Depth Buffer = Z-Axis - Per-pixel floating-point data for the z depth of each pixel rendered
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Ensure the matrix is reset
	glLoadIdentity();

	//
	gluLookAt(0.0f, 0.0f, 9.0f,
			  0.0f, 0.0f, -1.0f,
			  0.0f, 1.0f, 0.0f);

	//Draw the particle ball
	for (unsigned int i = 0; i < ballCount; i++)
		particleBall[i]->Draw();

	//Forces execution of OpenGL functions in finite time
	//As we are using double buffers, we use this instead of glFlush to do the above, but swap the buffers too
	glutSwapBuffers();
}

/**
 * Set up the rendering state
 */
void SetupRC(void)
{
	//Specifies the red, green, blue, and alpha values used by glClear to clear the color buffers
	//Set to blue (0.3f, 0.5f, 0.7f, 1.0f) but Black is needed for Spark
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//Enable depth testing
	//Depth testing: Each pixel coming into the depth buffer passes the depth test if its z value has the specified relation to the value 
	//already stored in the depth buffer
	glEnable(GL_DEPTH_TEST);

	//Set color shading model to flat
	//glShadeModel = selects flat or smooth shading
	//Look into flat and smooth shading
	glShadeModel(GL_SMOOTH);
}

/**
 * Called by GLUT library when the window has changed size
 */
void ChangeSize(int w, int h)
{
	//Viewport = the 2D rectangle used to project the 3D scene to the position of a virtual camera
	//Set Viewport to window dimensions (thus, taking up the entire window
    glViewport(0, 0, w, h);

	//glMatrixMode sets the current matrix mode we are manipulating
	//GL_PROJECTION = Applies subsequent matrix operations to the projection matrix stack.
	//Reset coordinate system
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();			//Set the Projection matrix to the Identity matrix (1)

	//gluPerspective = set up a perspective projection matrix
	//1 - fovy = Specifies the field of view angle, in degrees, in the y direction.
	//2 - aspect = Specifies the aspect ratio that determines the field of view in the x direction. The aspect ratio is the ratio of x (width) to y (height).
	//3 - zNear = Specifies the distance from the viewer to the near clipping plane (always positive).
	//4 - zFar = Specifies the distance from the viewer to the far clipping plane (always positive).
	gluPerspective(60.0, (GLfloat)w /(GLfloat)h, 1.0, 200.0);

	//Reset the MatrixMode to GL_MODELVIEW
	//GL_MODELVIEW = Applies subsequent matrix operations to the modelview matrix stack.
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

/**
 * Animate the cube rotating
 */
void Animate()
{
	//Update the particle ball
	for (unsigned int i = 0; i < ballCount; i++)
		particleBall[i]->Update();

	//Refresh the Window
	//glutPostRedisplay = Mark the normal plane of current window as needing to be redisplayed. The next iteration through glutMainLoop, 
	//the window's display callback will be called to redisplay the window's normal plane
	glutPostRedisplay();
}

/**
 * Take input from the keyboard for generic keyboard buttons
 */
void keyboardFunc(unsigned char key, int x, int y)
{
	//Check which key is pressed, ASCII style
	switch (key)
	{
		case 27:			//ESCAPE key
			exit(0);				//Close the game
			break;
	}
}

/**
 * Function called when an option is hit in the menu
 */
void Menu(int item)
{
	//Check which menu option we hit in the menu
	switch (item)
	{
		/**********************   Escaping Particles   ********************/

		case START_ESCAPING_PARTICLES:
			for (unsigned int i = 0; i < 3; i++)
				particleBall[i]->SetParticleEscape(true);
			break;

		case STOP_ESCAPING_PARTICLES:
			for (unsigned int i = 0; i < 3; i++)
				particleBall[i]->SetParticleEscape(false);
			break;

		/**********************   Number Of Balls   ********************/

		case BALL_COUNT_1:
			ballCount = 1;
			break;

		case BALL_COUNT_2:
			ballCount = 2;
			break;

		case BALL_COUNT_3:
			ballCount = 3;
			break;

		/**********************   Rotation Speed   ********************/

		case ROTATION_SLOW:
			for (unsigned int i = 0; i < 3; i++)
				particleBall[i]->SetRotationSpeed(0.1f);
			break;

		case ROTATION_MED:
			for (unsigned int i = 0; i < 3; i++)
				particleBall[i]->SetRotationSpeed(0.3f);
			break;

		case ROTATION_FAST:
			for (unsigned int i = 0; i < 3; i++)
				particleBall[i]->SetRotationSpeed(0.5f);
			break;

		/**********************   Texture Switch   ********************/

		case TEXTURE_CLOUD:
			for (unsigned int i = 0; i < 3; i++)
				particleBall[i]->SetTexture(textures[0]);
			break;

		case TEXTURE_BUBBLE:
			for (unsigned int i = 0; i < 3; i++)
				particleBall[i]->SetTexture(textures[1]);
			break;

		case TEXTURE_SPARK:
			for (unsigned int i = 0; i < 3; i++)
				particleBall[i]->SetTexture(textures[2]);
			break;

		/**********************   Color Switching   ********************/

		case COLOR_SET_1:
			particleBall[0]->SetColor(204.f, 0.f, 0.f, 0.f, 240.f, 0.f);
			particleBall[1]->SetColor(0.f, 65.f, 160.f, 240.f, 0.f, 0.f);
			particleBall[2]->SetColor(0.f, 65.f, 160.f, 240.f, 0.f, 0.f);
			break;

		case COLOR_SET_2:
			particleBall[0]->SetColor(0.f, 65.f, 160.f, 240.f, 0.f, 0.f);
			particleBall[1]->SetColor(0.f, 133.f, 0.f, 51.f, 0.f, 255.f);
			particleBall[2]->SetColor(0.f, 133.f, 0.f, 51.f, 0.f, 255.f);
			break;

		case COLOR_SET_3:
			particleBall[0]->SetColor(0.f, 133.f, 0.f, 51.f, 0.f, 255.f);
			particleBall[1]->SetColor(220.f, 82.f, 20.f, 102.f, 51.f, 102.f);
			particleBall[2]->SetColor(220.f, 82.f, 20.f, 102.f, 51.f, 102.f);
			break;

		/**********************   Close Window Command   ********************/

		case CLOSE_WINDOW:
			exit(0);
			break;
	}
}

/**
 * Setup the right click mennu
 */
void SetupMenu()
{
	//Create the escaping particles submenu
	int subMenu1 = glutCreateMenu(Menu);
	glutAddMenuEntry("Enable", START_ESCAPING_PARTICLES);
	glutAddMenuEntry("Disable", STOP_ESCAPING_PARTICLES);

	//Create the number of balls submenu
	int subMenu2 = glutCreateMenu(Menu);
	glutAddMenuEntry("1", BALL_COUNT_1);
	glutAddMenuEntry("2", BALL_COUNT_2);
	glutAddMenuEntry("3", BALL_COUNT_3);

	//Create the speed of rotation submenu
	int subMenu3 = glutCreateMenu(Menu);
	glutAddMenuEntry("Slow", ROTATION_SLOW);
	glutAddMenuEntry("Medium", ROTATION_MED);
	glutAddMenuEntry("Fast", ROTATION_FAST);

	//Create the texture switch submenu
	int subMenu4 = glutCreateMenu(Menu);
	glutAddMenuEntry("Cloud", TEXTURE_CLOUD);
	glutAddMenuEntry("Bubble", TEXTURE_BUBBLE);
	glutAddMenuEntry("Spark", TEXTURE_SPARK);

	//Create the color switch submenu
	int subMenu5 = glutCreateMenu(Menu);
	glutAddMenuEntry("Set 1", COLOR_SET_1);
	glutAddMenuEntry("Set 2", COLOR_SET_2);
	glutAddMenuEntry("Set 3", COLOR_SET_3);

	//Create the menu
	glutCreateMenu(Menu);

	//Add the rest of the menu items and the submenus
	glutAddSubMenu("Escaping Particles", subMenu1);
	glutAddSubMenu("No. Of Balls", subMenu2);
	glutAddSubMenu("Speed of Rotation", subMenu3);
	glutAddSubMenu("Texture", subMenu4);
	glutAddSubMenu("Color Sets", subMenu5);
	glutAddMenuEntry("Close System", CLOSE_WINDOW);

	//Associate a mouse button with menu
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

/**
 * Load all the textures into memory
 */
void LoadTextures()
{
	//Load the textures
	textures[0] = TextureLoader::LoadTexture("Cloud.bmp");
	textures[1] = TextureLoader::LoadTexture("Bubble.bmp");
	textures[2] = TextureLoader::LoadTexture("Spark.bmp");
}

/**
 * Unload all the textures from memory
 */
void UnloadTextures()
{
	//Unload the textures
	TextureLoader::UnloadTexture(textures[0]);
	TextureLoader::UnloadTexture(textures[1]);
	TextureLoader::UnloadTexture(textures[2]);
}

/**
 * Main program entry point
 */
int main(int argc, char* argv[])
{
	//Initialize the GLUT library and negotiate a session with the window system
	glutInit(&argc, argv);

	//Initialize the display mode
	//GLUT_SINGLE = single buffered window
	//GLUT_DOUBLE = double buffered window
	//GLUT_RGBA = selects the RGBA color model, but it does not request any bits of alpha be allocated. To request alpha, specify GLUT_ALPHA
	//GLUT_DEPTH = tells the GLUT library to allocate a depth buffer so depth testing can be performed
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);

	//Set the starting window size
	glutInitWindowSize(800, 600);

	//Create a new top-level window with a title
	glutCreateWindow("Particle Ball");

	//Sets the display callback for the current window (Calls RenderScene every loop)
	glutDisplayFunc(RenderScene);

	//Sets GLUT to call this method when the window changes size, to change the FOV of the user's window
	glutReshapeFunc(ChangeSize);

	//glutKeyboardFunc = keyboard callback for the current window
	glutKeyboardFunc(keyboardFunc);

	//glutIdleFunc = Continuously called when events are not being received (window moving, keyboard inputs, etc.)
	glutIdleFunc(Animate);

	//If we are displaying fullscreen, fullscreen it
	if (IS_FULLSCREEN)
		glutFullScreen();

	//Set up our rendering state
	SetupRC();

	//Setup the Right Click menu
	SetupMenu();

	//Initialize Texture Manager
	TextureLoader::Init();

	//Load the textures
	LoadTextures();

	//Initialize the particle ball
	particleBall = new ParticleBall*[3];
	particleBall[0] = new ParticleBall(textures[0], 300.f, 3.f, 0.3f, 0.0f, 0.0f, 0.0f, 204.f, 0.f, 0.f, 0.f, 240.f, 0.f);
	particleBall[1] = new ParticleBall(textures[0], 300.f, 3.f, 0.3f, -13.0f, 10.0f, -20.0f, 0.f, 65.f, 160.f, 240.f, 0.f, 0.f);
	particleBall[2] = new ParticleBall(textures[0], 300.f, 3.f, 0.3f, 13.0f, 10.0f, -20.0f, 0.f, 65.f, 160.f, 240.f, 0.f, 0.f);
	ballCount = 1;

	//Enters the GLUT event processing loop
	glutMainLoop();

	//Game over, destroy the particle ball
	for (unsigned int i = 0; i < 3; i++)
		delete particleBall[i];
	delete[] particleBall;

	//Unload the textures
	UnloadTextures();
	
	return 0;
}
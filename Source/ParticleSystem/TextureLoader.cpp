////////////////////////////////////////////////////////////////////////////////
// Filename: TextureLoader.cpp
////////////////////////////////////////////////////////////////////////////////

#include "TextureLoader.h"

/**
 * Initialize the Texture Loader
 */
void TextureLoader::Init()
{
	//Initialize DevIL

	//DevIL sanity check
	if ( (iluGetInteger(IL_VERSION_NUM) < IL_VERSION) || (iluGetInteger(ILU_VERSION_NUM) < ILU_VERSION) || (ilutGetInteger(ILUT_VERSION_NUM) < ILUT_VERSION) )
		return;

	//Initialise all DevIL functionality
	ilutRenderer(ILUT_OPENGL);
	ilInit();
	iluInit();
	ilutInit();
	ilutRenderer(ILUT_OPENGL);	//Tell DevIL that we're using OpenGL for our rendering
}

/**
 * Load a texture into OpenGL
 *
 * @filename	File to load (from SolnDir)
 */
GLuint TextureLoader::LoadTexture(const char* filename)
{
	ILuint imageID;		//Image IF
	GLuint textureID;	//OpenGL Texture ID
	ILboolean success;	//Error check
	ILenum error;		//Another error check!

	ilGenImages(1, &imageID); 			//Generate the image ID
	ilBindImage(imageID); 				//Bind the image
	success = ilLoadImage(filename);	//Load the image file

	//If the image was loaded correctly, manipulate it
	if (success)
	{
		//Ensure the image is flipped the right way up
		ILinfo ImageInfo;
		iluGetImageInfo(&ImageInfo);
		if (ImageInfo.Origin == IL_ORIGIN_UPPER_LEFT)
			iluFlipImage();

		//... then attempt to convert it.
		//NOTE: If your image contains alpha channel you can replace IL_RGB with IL_RGBA
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);

		// Quit out if we failed the conversion
		if (!success)
		{
			//error = ilGetError();
			//std::cout << "Image conversion failed - IL reports error: " << error << std::endl;
			exit(-1);
		}

		//Generate a new OpenGL texture
		glGenTextures(1, &textureID);

		//Bind the texture to a name
		glBindTexture(GL_TEXTURE_2D, textureID);

		//Set texture clamping method
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

		//Set texture interpolation method to use linear interpolation (no MIPMAPS)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		//Specify the texture specification
		glTexImage2D(GL_TEXTURE_2D, 				//Type of texture
					 0,								//Pyramid level (for mip-mapping) - 0 is the top level
					 ilGetInteger(IL_IMAGE_BPP),	//Image colour depth
					 ilGetInteger(IL_IMAGE_WIDTH),	//Image width
					 ilGetInteger(IL_IMAGE_HEIGHT),	//Image height
					 0,								//Border width in pixels (can either be 1 or 0)
					 ilGetInteger(IL_IMAGE_FORMAT),	//Image format (i.e. RGB, RGBA, BGR etc.)
					 GL_UNSIGNED_BYTE,				//Image data type
					 ilGetData());					//The actual image data itself
 	}
	//If the image loading failed
  	else
  	{
		//error = ilGetError();
		//std::cout << "Image load failed - IL reports error: " << error << std::endl;
		exit(-1);
  	}

 	ilDeleteImages(1, &imageID); //Because we have already copied image data into texture data we can release memory used by image.

	return textureID; //Return the GLuint to the texture so you can use it!
}

/**
 * Unload a texture from OpenGL
 *
 * @texture		Texture to unload
 */
void TextureLoader::UnloadTexture(GLuint texture)
{
	//Yay for one line!
	glDeleteTextures(1, &texture);
}
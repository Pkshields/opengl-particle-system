////////////////////////////////////////////////////////////////////////////////
// Filename: ParticleManager.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "BaseParticleManager.h"
#include "Particle.h"

using namespace std;

class ParticleManager : public BaseParticleManager
{
public:
	ParticleManager(GLuint texture);
	~ParticleManager();

	bool CreateParticle(float posX, float posY, float posZ, float velX, float velY, float velZ, float speed, float colorR, float colorG, float colorB, float colorA, float lifetime);
	
	list<BaseParticle*>* GetParticleList() { return &m_particleList; }
};
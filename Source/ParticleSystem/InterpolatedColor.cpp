////////////////////////////////////////////////////////////////////////////////
// Filename: InterpolatedColor.cpp
////////////////////////////////////////////////////////////////////////////////

#include "InterpolatedColor.h"

/**
 * Constructor for the Color Interpolation object
 */
InterpolatedColor::InterpolatedColor()
{
	m_initialColor.R = 0.0f;
	m_initialColor.G = 0.0f;
	m_initialColor.B = 0.0f;
	m_initialColor.A = 0.0f;

	m_currentColor.R = 0.0f;
	m_currentColor.G = 0.0f;
	m_currentColor.B = 0.0f;
	m_currentColor.A = 0.0f;

	m_deltaColor.R = 0.0f;
	m_deltaColor.G = 0.0f;
	m_deltaColor.B = 0.0f;
	m_deltaColor.A = 0.0f;
}

/**
 * Copy Constructor for the InterpolatedColor
 *
 * @copy		InterpolatedColor to copy
 */
InterpolatedColor::InterpolatedColor(const InterpolatedColor &copy)
{
	m_initialColor.R = copy.m_initialColor.R;
	m_initialColor.G = copy.m_initialColor.G;
	m_initialColor.B = copy.m_initialColor.B;
	m_initialColor.A = copy.m_initialColor.A;

	m_currentColor.R = copy.m_currentColor.R;
	m_currentColor.G = copy.m_currentColor.G;
	m_currentColor.B = copy.m_currentColor.B;
	m_currentColor.A = copy.m_currentColor.A;

	m_deltaColor.R = copy.m_deltaColor.R;
	m_deltaColor.G = copy.m_deltaColor.G;
	m_deltaColor.B = copy.m_deltaColor.B;
	m_deltaColor.A = copy.m_deltaColor.A;
}

/**
 * Destructor for the Color Interpolation object
 */
InterpolatedColor::~InterpolatedColor()
{
}

/**
 * Initialize the Color Interpolation object
 *
 * @initColorR		Red value of the initial color
 * @initColorG		Green value of the initial color
 * @initColorB		Blue value of the initial color
 * @finalColorR		Red value of the final color
 * @finalColorG		Green value of the final color
 * @finalColorB		Blue value of the final color
 */
bool InterpolatedColor::Initialize(int initColorR, int initColorG, int initColorB, int finalColorR, int finalColorG, int finalColorB)
{
	//Set the initial color 
	m_initialColor.R = initColorR / 255.f;
	m_initialColor.G = initColorG / 255.f;
	m_initialColor.B = initColorB / 255.f;
	m_initialColor.A = 1.0f;

	//Set the initial color to the initial color
	m_currentColor.R = initColorR / 255.f;
	m_currentColor.G = initColorG / 255.f;
	m_currentColor.B = initColorB / 255.f;
	m_currentColor.A = 1.0f;

	//Calculate the delta color
	m_deltaColor.R = (finalColorR / 255.f) - m_initialColor.R;
	m_deltaColor.G = (finalColorG / 255.f) - m_initialColor.G;
	m_deltaColor.B = (finalColorB / 255.f) - m_initialColor.B;
	m_deltaColor.A = 0.0f;

	return true;
}

/**
 * Update the color as per the linear interpolation
 *
 * @newPosition	New position - between 0 and 1 - in the interpolation for the color to be at
 */
bool InterpolatedColor::UpdateColor(float newPosition)
{
	//Ensure newPosition does not break the limits of 0..1
	if (newPosition < 0.f)
		newPosition = 0.f;
	else if (newPosition > 1.f)
		newPosition = 1.f;

	//Calculate the new color using the delta color
	m_currentColor.R = m_initialColor.R + (m_deltaColor.R * newPosition);
	m_currentColor.G = m_initialColor.G + (m_deltaColor.G * newPosition);
	m_currentColor.B = m_initialColor.B + (m_deltaColor.B * newPosition);

	return true;
}
////////////////////////////////////////////////////////////////////////////////
// Filename: ParticleBall.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include <windows.h>
#include <GL/GL.h>
#include <math.h>
#include "BallParticleManager.h"
#include "PSMath.h"
#include "Vector3f.h"

#define MAX_SPEED 0.45f
#define PARTICLE_CHECK_NUM 10.f
#define MAX_PARTICLE_LIFE 15000.f
#define RANDOM_DIRECTION (RANDOM_NUM * 2.0f) - 1.0f

//Get the current System time in seconds
#define CURRENT_TIME (static_cast<float>(timeGetTime()))

class ParticleBall
{
public:
	ParticleBall(GLuint texture, float particlesPerSecond, float ballRadius, float rotationSpeed, float posX, float posY, float posZ);
	ParticleBall(GLuint texture, float particlesPerSecond, float ballRadius, float rotationSpeed, float posX, float posY, float posZ, float initParticleColorR, float initParticleColorG, float initParticleColorB, float finalParticleColorR, float finalParticleColorG, float finalParticleColorB);
	~ParticleBall();

	bool Update();
	bool Draw();

	void SetParticleEscape(bool escape) { m_canParticlesEscape = escape; }
	void SetRotationSpeed(float speed) { m_rotationSpeed = speed; }
	void SetTexture(GLuint texture) { m_particleManager->SetTexture(texture); }
	void SetColor(float initParticleColorR, float initParticleColorG, float initParticleColorB, float finalParticleColorR, float finalParticleColorG, float finalParticleColorB)
	{
		m_initParticleColor.R = initParticleColorR;
		m_initParticleColor.G = initParticleColorG;
		m_initParticleColor.B = initParticleColorB;
		m_finalParticleColor.R = finalParticleColorR;
		m_finalParticleColor.G = finalParticleColorG;
		m_finalParticleColor.B = finalParticleColorB;
	}

private:
	void CreateNewParticles(const float timeElapsed);
	void ParticleBallCheck();

private:
	BallParticleManager* m_particleManager;

	float m_particlesPerSecond;
	float m_timeSinceLastParticle;

	unsigned int m_timeLastUpdate;

	float m_ballRadius;
	float m_rotationSpeed;
	float m_currentRotation;

	Vector3f m_position;

	ColorRGBA m_initParticleColor;
	ColorRGBA m_finalParticleColor;

	bool m_canParticlesEscape;
};
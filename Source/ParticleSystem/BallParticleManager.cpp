////////////////////////////////////////////////////////////////////////////////
// Filename: BallParticleManager.cpp
////////////////////////////////////////////////////////////////////////////////

#include "BallParticleManager.h"

/**
 * Constructor for the Ball Particle Manager
 *
 * textureName		Texture to use for these particles
 */
BallParticleManager::BallParticleManager(GLuint texture)
	: BaseParticleManager(texture)
{ }

/**
 * Destructor for the Base Particle Manager
 */
BallParticleManager::~BallParticleManager()
{ }

/**
 * Update the Particle Manager
 */
bool BallParticleManager::Update(const float timeElapsed)
{
	//Call the base Update first
	BaseParticleManager::Update(timeElapsed);

	return true;
}

/**
 * Create a particle for the Particle Manager
 */
bool BallParticleManager::CreateParticle(float posX, float posY, float posZ, float velX, float velY, float velZ, float speed, float initColorR, float initColorG, float initColorB, float finalColorR, float finalColorG, float finalColorB, float lifetime, float maxTravel)
{
	//Variables
	BallParticle* particle;		//Particle holder

	//If the queue of dead particles is not empty
	if (!m_deadParticles.empty())
	{
		//Get the particle from the head of the queue and remove it from the queue
		particle = static_cast<BallParticle*>(m_deadParticles.front());
		m_deadParticles.pop();

		//Reinitialize it the add it to the alive list
		particle->Initialize(posX, posY, posZ, velX, velY, velZ, speed, initColorR, initColorG, initColorB, finalColorR, finalColorG, finalColorB, lifetime, maxTravel);
		m_particleList.push_back(particle);

		return true;
	}
	else
	{
		//Else, if we have not hit the maximum number of particles...
		if (m_particleList.size() < MAX_PARTICLES)
		{
			//... Create a new particle, initialize it and add it to the alive list
			particle = new BallParticle();
			particle->Initialize(posX, posY, posZ, velX, velY, velZ, speed, initColorR, initColorG, initColorB, finalColorR, finalColorG, finalColorB, lifetime, maxTravel);
			m_particleList.push_back(particle);

			return true;
		}
	}

	return false;
}

/**
 * Free a random particle from the Particle Ball
 */
bool BallParticleManager::FreeParticle()
{
	//Get a random particle and make ti fly away!
	//... Alright, it's the first in the list that's hit the max distance and is not already flying away
	BallParticle* particle;
	for (list<BaseParticle*>::reverse_iterator i = m_particleList.rbegin(); i != m_particleList.rend(); i++)
	{
		//Check if the particle is still moving. If it isn't then break it free then break out of the loop
		particle = static_cast<BallParticle*>(*i);
		if (!particle->IsStillMoving())
		{
			particle->BreakFree();
			break;
		}
	}

	return true;
}
////////////////////////////////////////////////////////////////////////////////
// Filename: BallParticle.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "BaseParticle.h"
#include "Vector3f.h"

class BallParticle : public BaseParticle
{
public:
	BallParticle();
	BallParticle(const BallParticle &copy);
	~BallParticle();

	bool Initialize(float posX, float posY, float posZ, float velX, float velY, float velZ, float speed, float initColorR, float initColorG, float initColorB, float finalColorR, float finalColorG, float finalColorB, float lifetime, float maxTravel);
	bool Update(float timeElapsed);
	bool BreakFree();

	bool GetHasMaxTravel() { return m_hasMaxTravel; }
	void SetHasMaxTravel(bool maxTravel) { m_hasMaxTravel = maxTravel; }
	bool IsStillMoving() { return m_isStillMoving; }

private:
	float m_maxTravel;
	bool m_hasMaxTravel;
	bool m_isStillMoving;
};


#include "BaseParticle.h"

/**
 * Constructor for the BaseParticle
 */
BaseParticle::BaseParticle()
{
	m_position.X = 0.0f;
	m_position.Y = 0.0f;
	m_position.Z = 0.0f;

	m_velocity.X = 0.0f;
	m_velocity.Y = 0.0f;
	m_velocity.Z = 0.0f;

	m_color = InterpolatedColor();

	m_age = -1;
	m_lifetime = -1;
	m_isAlive = false;
}

/**
 * Copy Constructor for the BaseParticle
 *
 * copy		BaseParticle to copy
 */
BaseParticle::BaseParticle(const BaseParticle &copy)
{
	m_position = copy.m_position;
	m_velocity = copy.m_velocity;
	m_speed = copy.m_speed;
	m_color = copy.m_color;
	m_age = copy.m_age;
	m_lifetime = copy.m_lifetime;
	m_isAlive = copy.m_isAlive;
}

/**
 * Destructor for the BaseParticle
 */
BaseParticle::~BaseParticle()
{
}

/**
 * Update the BaseParticle
 *
 * @param timeElapsed	Elapsed time since last particle update
 */
bool BaseParticle::Update(float timeElapsed)
{
	//If the particle is still alive
	if (m_age < m_lifetime)
	{
		//Update the age of the particle
		m_age += timeElapsed;

		//Update the position of the particle depending on the velocity
		m_position.X += m_velocity.X * timeElapsed * m_speed;
		m_position.Y += m_velocity.Y * timeElapsed * m_speed;
		m_position.Z += m_velocity.Z * timeElapsed * m_speed;

		//Update the alpha value for the particle
		m_color.SetAlpha(1.0f - (m_age / m_lifetime));
	}
	else
	{
		//Particle is dead. Kill it.
		m_isAlive = false;
	}

	return true;
}

/**
 * Draw the BaseParticle
 */
bool BaseParticle::Draw()
{
	//Draw a sprite at this position with this colour
	ColorRGBA color = (m_color.GetColor());
	glColor4f(color.R, color.G, color.B, color.A);
	glVertex3f(m_position.X, m_position.Y, m_position.Z);

	return true;
}
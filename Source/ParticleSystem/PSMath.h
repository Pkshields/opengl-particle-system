////////////////////////////////////////////////////////////////////////////////
// Filename: PSMath.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include <windows.h>

//A number between 0.0000 and 1.00000
#define RANDOM_NUM (static_cast<float>(rand())/static_cast<float>(RAND_MAX))

struct ColorRGBA
{
	float R;
	float G;
	float B;
	float A;
};
////////////////////////////////////////////////////////////////////////////////
// Filename: InterpolatedColor.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "PSMath.h"

class InterpolatedColor
{
public:
	InterpolatedColor();
	InterpolatedColor(const InterpolatedColor &copy);
	~InterpolatedColor();

	bool Initialize(int initColorR, int initColorG, int initColorB, int finalColorR, int finalColorG, int finalColorB);

	bool UpdateColor(float newPosition);

	ColorRGBA GetColor() { return m_currentColor; }
	void SetAlpha(float alpha) { m_currentColor.A = alpha; }

private:
	ColorRGBA m_initialColor;
	ColorRGBA m_currentColor;
	ColorRGBA m_deltaColor;
};
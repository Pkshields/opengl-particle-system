////////////////////////////////////////////////////////////////////////////////
// Filename: ParticleManager.cpp
////////////////////////////////////////////////////////////////////////////////

#include "ParticleManager.h"

/**
 * Constructor for the Particle Manager
 *
 * texture		Texture to use for these particles
 */
ParticleManager::ParticleManager(GLuint texture)
	: BaseParticleManager(texture)
{ }

/**
 * Destructor for the Particle Manager
 */
ParticleManager::~ParticleManager()
{ }

/**
 * Create a particle for the Particle Manager
 */
bool ParticleManager::CreateParticle(float posX, float posY, float posZ, float velX, float velY, float velZ, float speed, float colorR, float colorG, float colorB, float colorA, float lifetime)
{
	//Variables
	Particle* particle;		//Particle holder

	//If the queue of dead particles is not empty
	if (!m_deadParticles.empty())
	{
		//Get the particle from the head of the queue and remove it from the queue
		particle = static_cast<Particle*>(m_deadParticles.front());
		m_deadParticles.pop();

		//Reinitialize it the add it to the alive list
		particle->Initialize(posX, posY, posZ, velX, velY, velZ, speed, colorR, colorG, colorB, colorA, lifetime);
		m_particleList.push_back(particle);

		return true;
	}
	else
	{
		//Else, if we have not hit the maximum number of particles...
		if (m_particleList.size() < MAX_PARTICLES)
		{
			//... Create a new particle, initialize it and add it to the alive list
			particle = new Particle();
			particle->Initialize(posX, posY, posZ, velX, velY, velZ, speed, colorR, colorG, colorB, colorA, lifetime);
			m_particleList.push_back(particle);

			return true;
		}
	}

	return false;
}
////////////////////////////////////////////////////////////////////////////////
// Filename: BaseParticle.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include <Windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "InterpolatedColor.h"
#include "Vector3f.h"

class BaseParticle
{
public:
	BaseParticle();
	BaseParticle(const BaseParticle &copy);
	~BaseParticle();

	virtual bool Update(float timeElapsed);
	virtual bool Draw();

	bool IsAlive() { return m_isAlive; }

	void SetPosition(Vector3f position)		{ m_position = position; }
	Vector3f GetPosition()					{ return m_position; }

	void SetVelocity(Vector3f velocity)		{ m_velocity = velocity; }
	Vector3f GetVelocity()					{ return m_velocity; }

	void SetSpeed(float speed)				{ m_speed = speed; }
	float GetSpeed()						{ return m_speed; }

	InterpolatedColor* GetColor()			{ return &m_color; }

protected:
	Vector3f m_position;
	Vector3f m_velocity;
	float m_speed;
	InterpolatedColor m_color;
	float m_age;
	float m_lifetime;
	bool m_isAlive;
};


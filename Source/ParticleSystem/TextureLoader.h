////////////////////////////////////////////////////////////////////////////////
// Filename: TextureLoader.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include <Windows.h>		//GL/UT includes
#include <GL/gl.h>
#include <GL/glu.h>

#define ILUT_USE_OPENGL		//This MUST be defined before calling the DevIL headers or we don't get OpenGL functionality
#include <IL/il.h>			//DevIl Includes
#include <IL/ilu.h>
#include <IL/ilut.h>

#include <stdio.h>			//Standard includes
#include <stdlib.h>
#include <string.h>
#include <iostream>

class TextureLoader
{
public:
	static void Init();
	static GLuint LoadTexture(const char* file);
	static void UnloadTexture(GLuint texture);

private:
	TextureLoader(void);
	~TextureLoader(void);
};
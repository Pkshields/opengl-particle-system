////////////////////////////////////////////////////////////////////////////////
// Filename: ParticleBall.cpp
////////////////////////////////////////////////////////////////////////////////

#include "ParticleBall.h"

/**
 * Constructor for the Particle Ball
 */
ParticleBall::ParticleBall(GLuint texture, float particlesPerSecond, float ballRadius, float rotationSpeed, float posX, float posY, float posZ)
{
	//Set the initial "last time" the system was updated
	m_timeLastUpdate = CURRENT_TIME;

	//Create and Initialze the Particle Manager
	m_particleManager = new BallParticleManager(texture);

	//Store passed in settings
	m_particlesPerSecond = particlesPerSecond;
	m_ballRadius = ballRadius;
	m_rotationSpeed = rotationSpeed;
	m_currentRotation = 0.0f;
	m_timeSinceLastParticle = 0.0f;
	m_timeLastUpdate = 0.0f;
	m_position = Vector3f(posX, posY, posZ);
	m_canParticlesEscape = false;

	//Store the color settings
	m_initParticleColor.R = 204.f;
	m_initParticleColor.G = 0.f;
	m_initParticleColor.B = 0.f;
	m_finalParticleColor.R = 80.f;
	m_finalParticleColor.G = 255.f;
	m_finalParticleColor.B = 0.f;
}

/**
 * Constructor for the Particle Ball
 */
ParticleBall::ParticleBall(GLuint texture, float particlesPerSecond, float ballRadius, float rotationSpeed, float posX, float posY, float posZ,
						   float initParticleColorR, float initParticleColorG, float initParticleColorB, float finalParticleColorR, float finalParticleColorG, float finalParticleColorB)
{
	//Set the initial "last time" the system was updated
	m_timeLastUpdate = CURRENT_TIME;

	//Create and Initialze the Particle Manager
	m_particleManager = new BallParticleManager(texture);

	//Store passed in settings
	m_particlesPerSecond = particlesPerSecond;
	m_ballRadius = ballRadius;
	m_rotationSpeed = rotationSpeed;
	m_currentRotation = 0.0f;
	m_timeSinceLastParticle = 0.0f;
	m_timeLastUpdate = 0.0f;
	m_position = Vector3f(posX, posY, posZ);
	m_canParticlesEscape = false;
	
	//Store the color settings
	m_initParticleColor.R = initParticleColorR;
	m_initParticleColor.G = initParticleColorG;
	m_initParticleColor.B = initParticleColorB;
	m_finalParticleColor.R = finalParticleColorR;
	m_finalParticleColor.G = finalParticleColorG;
	m_finalParticleColor.B = finalParticleColorB;
}

/**
 * Destructor for the Particle Ball
 */
ParticleBall::~ParticleBall()
{
	//Destroy and delete the Particle Manager
	delete m_particleManager;
}

/**
 * Update the Particle Ball
 */
bool ParticleBall::Update()
{
	//Calculate the time since last update, store the current time as last time
	float currentTime = CURRENT_TIME;
	float timeElapsed = currentTime - m_timeLastUpdate;
	m_timeLastUpdate = currentTime;

	//Create new particles, if we can.
	CreateNewParticles(timeElapsed);

	//Update the Particle Manager
	m_particleManager->Update(timeElapsed);

	//Run code to check certain options for the Particle Ball
	ParticleBallCheck();

	return true;
}

/**
 * Draw the particles for the Particle Ball
 */
bool ParticleBall::Draw()
{
	//Store current matrix
	glPushMatrix();

	//Move ball out to the position it needs to be drawwn at
	glTranslatef(m_position.X, m_position.Y, m_position.Z);

	//Rotate the ball
	m_currentRotation = fmodf((m_currentRotation + m_rotationSpeed), 360.f);
	glRotatef(m_currentRotation, 0, 1, 0);

	//Draw the particles in the Particle Manager
	m_particleManager->Draw();

	//Pop above matrix
	glPopMatrix();

	return true;
}

/**
 * Draw the particles for the Particle Ball
 * 
 * @param timeElapsed	Elapsed time tince last update
 */
void ParticleBall::CreateNewParticles(const float timeElapsed)
{
	//Calculate the number of particles to create this update
	m_timeSinceLastParticle += timeElapsed;
	unsigned int particlesToCreate = (m_timeSinceLastParticle / 1000.f) * m_particlesPerSecond;

	//If we are creating particles, reset the Time Since variable
	if (particlesToCreate != 0)
		m_timeSinceLastParticle = 0.0f;

	//Loop to create all particles needed
	while (particlesToCreate != 0)
	{
		//Create a particle randomly
		m_particleManager->CreateParticle(0.0f, 
										  0.0f, 
										  0.0f,  
										  RANDOM_DIRECTION, 
										  RANDOM_DIRECTION, 
										  RANDOM_DIRECTION, 
										  (RANDOM_NUM * MAX_SPEED), 
										  m_initParticleColor.R,
										  m_initParticleColor.G, 
										  m_initParticleColor.B, 
										  m_finalParticleColor.R,
										  m_finalParticleColor.G,
										  m_finalParticleColor.B,
										  (RANDOM_NUM * MAX_PARTICLE_LIFE), 
										  m_ballRadius);

		//Decrement "particles to create" variable
		particlesToCreate--;
	}
}
/**
 * Checks the distance from center each particle is and restricts it to within the ball radius
 */
void ParticleBall::ParticleBallCheck()
{
	//If we are allowed to make particles escape...
	if (m_canParticlesEscape)
	{
		//Give me a random number between 1 and 7
		unsigned int number = RANDOM_NUM * PARTICLE_CHECK_NUM;

		//If we have hit the jackpot
		if (number == 1)
		{
			//Enable a random particle in the manager to fly away from the ball
			//... Alright, it's the first in the list that's hit the max distance and is not already flying away
			m_particleManager->FreeParticle();
		}
	}
}
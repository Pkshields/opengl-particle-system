////////////////////////////////////////////////////////////////////////////////
// Filename: BallParticleManager.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "BaseParticleManager.h"
#include "BallParticle.h"

class BallParticleManager : public BaseParticleManager
{
public:
	BallParticleManager(GLuint texture);
	~BallParticleManager();

	bool Update(const float timeElapsed);

	bool CreateParticle(float posX, float posY, float posZ, float velX, float velY, float velZ, float speed, float initColorR, float initColorG, float initColorB, float finalColorR, float finalColorG, float finalColorB, float lifetime, float maxTravel);
	bool FreeParticle();
};


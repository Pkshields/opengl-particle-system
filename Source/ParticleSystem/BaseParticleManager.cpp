////////////////////////////////////////////////////////////////////////////////
// Filename: BaseParticleManager.cpp
////////////////////////////////////////////////////////////////////////////////

#include "BaseParticleManager.h"

/**
 * Constructor for the Base Particle Manager
 *
 * texture		Texture to use for these particles
 */
BaseParticleManager::BaseParticleManager(GLuint texture)
{
	//Store the Particle texture
	m_texture = texture;
}

/**
 * Destructor for the Base Particle Manager
 */
BaseParticleManager::~BaseParticleManager()
{
	//Variables
	BaseParticle* particle;					//Particle holder
	list<BaseParticle*> deadParticlesList;	//List to contain the particles to remove from active rotation

	//Loop through the alive particles list
	for (list<BaseParticle*>::iterator i = m_particleList.begin(); i != m_particleList.end(); i++)
	{
		//Get the particle, destroy it then delete it
		particle = *i;
		delete particle;
	}

	//Loop through the list of dead particles
	while(!m_particleList.empty())
	{
		//Get the next particle, remove it from the list, destroy it then delete it
		particle = m_deadParticles.front();
		m_deadParticles.pop();
		delete particle;
	}
}

/**
 * Update the ParticleManager
 */
bool BaseParticleManager::Update(const float timeElapsed)
{
	//Variables
	BaseParticle* particle;					//Particle holder
	list<BaseParticle*> deadParticlesList;	//List to contain the particles to remove from active rotation
	
	//Loop through the alive particles list
	for (list<BaseParticle*>::iterator i = m_particleList.begin(); i != m_particleList.end(); i++)
	{
		//Get the particle from the list and update it
		particle = *i;
		particle->Update(timeElapsed);
		
		//If the particle is no longer alive, add it to the dead list
		if (!particle->IsAlive())
		{
			deadParticlesList.push_back(particle);
		}
	}

	//Loop through the dead list
	for (list<BaseParticle*>::iterator i = deadParticlesList.begin(); i != deadParticlesList.end(); i++)
	{
		//Remove them from the active list and ad it to the dead particle queue
		particle = *i;
		m_particleList.remove(particle);
		m_deadParticles.push(particle);
	}

	return true;
}

/**
 * Draw the particles from the Base Particle Manager
 */
bool BaseParticleManager::Draw()
{
	//Variables
	BaseParticle* particle;		//Particle holder

	/******************************************************************************/

	//Make space for particle limits and fill it from OGL call.
	GLfloat sizes[2];
	glGetFloatv(GL_ALIASED_POINT_SIZE_RANGE, sizes);

	//Enable our texture type (in this case, it must be GL_TEXTURE_2D)
	glEnable(GL_TEXTURE_2D);

	//Bind the texture data
	glBindTexture(GL_TEXTURE_2D, m_texture);

	//Enable blending and set for particles
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);//GL_ONE_MINUS_SRC_ALPHA

	//?????
	glPointParameterfARB  = (PFNGLPOINTPARAMETERFARBPROC)wglGetProcAddress("glPointParameterfARB");
	glPointParameterfvARB = (PFNGLPOINTPARAMETERFVARBPROC)wglGetProcAddress("glPointParameterfvARB");

	//Enable point sprites and set params for points.
	glEnable(GL_POINT_SPRITE_ARB);
	float quadratic[] =  { 1.0f, 0.0f, 0.01f };
	glPointParameterfvARB( GL_POINT_DISTANCE_ATTENUATION_ARB, quadratic );
	glPointParameterfARB( GL_POINT_FADE_THRESHOLD_SIZE_ARB, 60.0f );

	//Tell it the max and min sizes we can use using our pre-filled array.
	glPointParameterfARB( GL_POINT_SIZE_MIN_ARB, sizes[0] );
	glPointParameterfARB( GL_POINT_SIZE_MAX_ARB, sizes[1] );

	//Tell OGL to replace the coordinates upon drawing.
	glTexEnvi(GL_POINT_SPRITE_ARB, GL_COORD_REPLACE_ARB, GL_TRUE);

	//Set the size of the points.
	glPointSize(32.0f);

	//Turn off depth masking so particles in front will not occlude particles behind them.
	glDepthMask(GL_FALSE);

	//Save the current transform.
	glPushMatrix();
	
	//Begin drawing points.
	glBegin(GL_POINTS);
	
	//Loop through the alive particles list
	for (list<BaseParticle*>::iterator i = m_particleList.begin(); i != m_particleList.end(); i++)
	{
		//Get the particle and draw it
		particle = *i;
		particle->Draw();
	}
	
	glEnd(); //Stop drawing points.

	//Return to the previous matrix.
	glPopMatrix();

	return true;
}
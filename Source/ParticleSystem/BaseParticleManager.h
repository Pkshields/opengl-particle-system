////////////////////////////////////////////////////////////////////////////////
// Filename: BaseParticleManager.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "glext.h"

#include <list>
#include <queue>

#include "TextureLoader.h"
#include "BaseParticle.h"

//Maximum number of particles the system can handle
#define MAX_PARTICLES 4000

using namespace std;

class BaseParticleManager
{
public:
	BaseParticleManager(GLuint texture);
	~BaseParticleManager();

	virtual bool Update(const float timeElapsed);
	virtual bool Draw();

	void SetTexture(GLuint texture) { m_texture = texture; }

protected:
	list<BaseParticle*> m_particleList;
	queue<BaseParticle*> m_deadParticles;

	GLuint m_texture;

	PFNGLPOINTPARAMETERFARBPROC glPointParameterfARB;
	PFNGLPOINTPARAMETERFVARBPROC glPointParameterfvARB;
};


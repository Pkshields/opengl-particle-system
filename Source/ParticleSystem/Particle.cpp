////////////////////////////////////////////////////////////////////////////////
// Filename: Particle.cpp
////////////////////////////////////////////////////////////////////////////////

#include "Particle.h"

/**
 * Constructor for the Particle
 */
Particle::Particle()
	: BaseParticle()
{ }

/**
 * Copy Constructor for the Particle
 *
 * copy		Particle to copy
 */
Particle::Particle(const Particle &copy)
	: BaseParticle(copy)
{ }


/**
 * Destructor for the Particle
 */
Particle::~Particle()
{ }

/**
 * Initialize the Particle
 *
 * @param posX		X position of the particle
 * @param posY		Y position of the particle
 * @param posZ		Z position of the particle
 * @param velX		Velocity in the X direction of the particle
 * @param velY		Velocity in the Y direction of the particle
 * @param velZ		Velocity in the Z direction of the particle
 * @param colorR	Red value for the color of the particle
 * @param colorG	Green value for the color of the particle
 * @param colorB	Blue value for the color of the particle
 * @param colorA	Alpha value for the color of the particle
 * @param lifetime	Total lifetime for the particle
 */
bool Particle::Initialize(float posX, float posY, float posZ, float velX, float velY, float velZ, float speed, float colorR, float colorG, float colorB, float colorA, float lifetime)
{
	m_position.X = posX;
	m_position.Y = posY;
	m_position.Z = posZ;

	m_velocity.X = velX;
	m_velocity.Y = velY;
	m_velocity.Z = velZ;
	m_speed = speed / 100;

	m_color.Initialize(204, 0, 0, 80, 255, 0);//(colorR, colorG, colorB, colorR, colorG, colorB);	//

	m_age = 0.0f;
	m_lifetime = lifetime;
	m_isAlive = true;

	return true;
}